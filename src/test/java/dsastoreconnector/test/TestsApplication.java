/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dsastoreconnector.test;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import dsastoreconnector.restapi.types.DSAMetadata;


public class TestsApplication {
	private static final Logger log = LoggerFactory.getLogger(TestsApplication.class);
	
	public static void main(String args[]) throws IOException {
        String url = "http://localhost:8080/v1";        
        
        String dsaDocPath = "./DSA-f357cc3f-fdab-45c9-9eab-1a50fd3d6143.xml";
        
        
        //read xml doc into byte[], filename is filepath string
        /*
        BufferedReader br = new BufferedReader(new FileReader(new File(dsaDocPath)));
        String line;
        StringBuilder dsaDoc = new StringBuilder();
        while((line= br.readLine())!= null){
        	dsaDoc.append(line.trim());
        }
        br.close();
        */
        File file = new File(dsaDocPath);
        Resource resource = new FileSystemResource(file);
       
        MultiValueMap<String, Object> parameters = new LinkedMultiValueMap<String, Object>();
        parameters.add("DSAFile", resource);
        
        //using restTemplate exchange
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<MultiValueMap<String, Object>>(parameters, headers);
        RestTemplate template = new RestTemplate();
        ResponseEntity<String> response = template.exchange(url + "/createDSA", HttpMethod.PUT, requestEntity, String.class);
                             
        
        //RestTemplate template = new RestTemplate();
        //ResponseEntity<String> response = template.postForEntity(url + "/createDSA", parameters, String.class);
        String dsaId = null;
        if(response.getStatusCode()==HttpStatus.OK){
        	dsaId = response.getBody().toString();
        	log.info("DSA id is: " + dsaId);
        }
        
        
        /////////test readDSA
        RestTemplate restTemplate = new RestTemplate();
        String dsaId_t = "DSA-f357cc3f-fdab-45c9-9eab-1a50fd3d6143.xml";
        ResponseEntity<byte[]> respget = restTemplate.getForEntity(url + "/readDSA/{dsaId}", byte[].class, dsaId_t);
        
        if(respget.hasBody()==true){
        	log.info("Success retrieve. DSA is: " + respget.getBody().toString());	
        }else{
        	log.info("Fail retrieve. DSA ");
        }
        /*//Write the dsa content into a xml file
        byte[] dsaFile = respget.getBody();
        String fileDest = dsaId_t;                
        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            fileOuputStream.write(dsaFile);
            log.info("!!!!!!!!Success write dsaFile in xml");
        } catch (IOException e) {
            e.printStackTrace();
            log.info("!!!!!!!!Fail write dsaFile in xml");
        }
        */
        
        /*
        //////test updateDSA
        dsaDocPath = "./DSA-f357cc3f-fdab-45c9-9eab-1a50fd3d6142.xml";
        File fileUpdate = new File(dsaDocPath);
        Resource resourceUpdate = new FileSystemResource(fileUpdate);
       
        MultiValueMap<String, Object> parametersUpdate = new LinkedMultiValueMap<String, Object>();
        parametersUpdate.add("DSAFile", resourceUpdate);
        
        RestTemplate template_update = new RestTemplate();
        template_update.put(url + "/updateDSA", parametersUpdate);
        //template.patchForObject(url + "/updateDSA/{dsaId}", dsa, String.class, dsaId_t);
        //log.info("Success update. DSA id is");
        */
        
        /////test searchDSA
        String searchString = "a JSON search string";
        RestTemplate templateSearch = new RestTemplate();
        ResponseEntity<List> responseSearch = templateSearch.postForEntity(url + "/searchDSA/{longResultFlag}", searchString, List.class, true);
        if(responseSearch.getStatusCode()==HttpStatus.OK){
        	for(int i = 0; i < responseSearch.getBody().size(); i++){
        		log.info("search result: " + i + " " + responseSearch.getBody().get(i));	
        	}
        	
        }
        responseSearch = templateSearch.postForEntity(url + "/searchDSA/{longResultFlag}", searchString, List.class, false);
        if(responseSearch.getStatusCode()==HttpStatus.OK){
        	for(int i = 0; i < responseSearch.getBody().size(); i++){
        		log.info("search result: " + i + " " + responseSearch.getBody().get(i));	
        	}
        	
        }
        
        /*
        //////test deleteDSA  
        restTemplate.delete(url + "/deleteDSA/{dsaId}", dsaId_t);
        log.info("success delete DSA: " + dsaId_t);
        */       
    }    

}
