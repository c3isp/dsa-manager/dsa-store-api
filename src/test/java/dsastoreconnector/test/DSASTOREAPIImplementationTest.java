/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dsastoreconnector.test;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import dsastoreconnector.ApplicationDeployer;
import dsastoreconnector.restapi.impl.DSASTOREAPIImplementation;

// for code completion add MockMvcRequestBuilders and MockMvcRequestBuilders as 'favorite types' in eclipse
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*; //get, post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;

import static org.hamcrest.CoreMatchers.*;

/**
 * @author MIMANE
 *
 */
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes=ApplicationDeployer.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("test") // load application-test.properties
@AutoConfigureMockMvc
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DSASTOREAPIImplementationTest {

    @Autowired
    private MockMvc mockMvc;
    
    @Value("${security.user.name}")
    private String restUser;
    @Value("${security.user.password}")
    private String restPassword;
    
    @Test
    public void readDSA() throws Exception {
        
        
        String dsaId = "DSA-f357cc3f-fdab-45c9-9eab-1a50fd3d6143.xml";
        /*
        //System.out.println(">>>>>>>>>>>"+restUser);
        this.mockMvc.perform(
                get("/v1/readDSA/" + dsaId )
                    //.with(httpBasic(restUser, restPassword)) // basic auth
                    //.accept(MediaType.APPLICATION_JSON)
                    //.contentType(MediaType.APPLICATION_JSON)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                //.andExpect(content().string(containsString(expectedOutput))) //check if the returned output (json) contains 'expectedOutput
                ;
        */
    }
    
    /*
	@Test
    public void test01get() throws Exception {
        
        String param = "test";
        String expectedOutput = "aName";
        
        System.out.println(">>>>>>>>>>>"+restUser);
        this.mockMvc.perform(
                get("/v1/template/" + param + "/")
                    .with(httpBasic(restUser, restPassword)) // basic auth
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(expectedOutput))) //check if the returned output (json) contains 'expectedOutput
                ;
    }
	
	@Test
    public void test02get() throws Exception {
        
        String param = "test";
        //expected output structure = "{\"name\":\"aName\",\"id\":\"anIdValue\",\"path\":\"aPath\",\"version\":\"1\"}";
        
        this.mockMvc.perform(
                get("/v1/template/" + param + "/")
                    .with(httpBasic(restUser, restPassword)) // basic auth
                    .header("X-myheader-test1", "TEST1")
                    .header("X-myheader-test2", "TEST2")
                    .accept(MediaType.APPLICATION_JSON)
                    .contentType(MediaType.APPLICATION_JSON)
                 )
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("aName")))
                ;
        
    }
    */
}
