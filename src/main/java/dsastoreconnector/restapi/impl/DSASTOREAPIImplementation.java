/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dsastoreconnector.restapi.impl;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import dsastoreconnector.restapi.types.DSAQuery;
import dsastoreconnector.restapi.types.DSAMetadata;

//import com.google.common.io.Files;

//JZ: libraries for validation
import javax.xml.XMLConstants;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;


import dsastoreconnector.restapi.types.DSAXML;
import dsastoreconnector.restapi.types.DSAMetadata;

/**
 * 
 * @author Wenjun Fan, Joanna Ziembicka
 * 
 * This is the main implementation class of the DSA Store REST API
 *
 */



//@ApiModel(value = "Template", description = "Template of REST APIs")
@ApiModel(value = "DSAstore", description = "DSA STORE REST APIs")
@RestController
@RequestMapping("/v1")
//@RequestMapping("/")
public class DSASTOREAPIImplementation {

	/**
	 * These two are examples of configuration extracted from the
	 * application.properties file under the src/main/resources. Springboot will
	 * automatically load the file and its values, if file is not in the local
	 * path, it can be in the java classpath or set as an environment variable
	 */
	
	//@Value("${rest.endpoint.url.callGet}")
	//private String callGetEndpoint;

	@Value("${security.user.name}")
	private String restUser;
	@Value("${security.user.password}")
	private String restPassword;
	
	//JZ: Added a DSA schema
	@Value("${DSA_SCHEMA_FILENAME}")
	private String DSA_SCHEMA_FILENAME;
	
	//JZ: Added an attribute config file
	@Value("${DSA_ATTRIBUTE_CONFIG_FILENAME}")
	private String DSA_ATTRIBUTE_CONFIG_FILENAME;
	
	
	/**
	 * Used to call REST endpoints; configured by RestTemplateBuilder
	 */
	
	@Autowired
	private DSAStore dsaStore;
	
	@Autowired
	private DSAMetadataOntology ontology;
	
	@Autowired
	private RestTemplate restTemplate;
	@Bean
    public RestTemplate restTemplate(RestTemplateBuilder restTemplateBuilder) {
    	return restTemplateBuilder.basicAuthorization(restUser, restPassword).build();
    }
	
	
	/**
	 * Logger class, can be used within the methods to log information
	 */
	private final static Logger LOGGER = LoggerFactory.getLogger(DSASTOREAPIImplementation.class);
    
	/**
	 * Helper methods 
	 */
	
	// JZ: Validate the XML document based on the filename in DSA_SCHEMA_FILENAME
		/*
		 * Validates an XML document against an XSD in DSA_SCHEMA_FILENAME
		 * @param doc a DOM Document object containing an XML file
		 * @return true if xml file is valid, false if invalid
		 */
		private Boolean validateXMLDoc(Document doc) {
			//TODO: outside of this method, read in the XSD, check if the xsd file exists, 
			//		and keep it as a schema in memory instead of reading it every time
			//		Move it to a new constructor

			SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);        
			//String schemaFilename = DSA_SCHEMA_FILENAME;
			//WJ: read the file from classpath
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(DSA_SCHEMA_FILENAME);
			Source schemaSource = new StreamSource(inputStream);
			try {
				//Schema schema = schemaFactory.newSchema(new File(schemaFilename));
				Schema schema = schemaFactory.newSchema(schemaSource);
				Validator validator = schema.newValidator();
				validator.validate(new DOMSource(doc.getDocumentElement()));
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		
		/** 
		 * Reads the DSA ontology file into a JSON Object
		 * <p>The DSA ontology file is configured in application properties as DSA_ATTRIBUTE_CONFIG_FILENAME</p>
		 * @return a JSONObject containing the DSA ontology
		 */
		private JSONObject readAttributeConfig() throws IOException {
			//TODO: outside of this method, verify file, read in the config, 
			//		and keep it as a string in memory instead of reading it every time
			//		Move it to a new constructor
			
			// Read in file 
			//File configFile = new File(DSA_ATTRIBUTE_CONFIG_FILENAME);
			//String configString = FileUtils.readFileToString(configFile); 
			//String configString = new String(Files.readAllBytes(Paths.get(DSA_ATTRIBUTE_CONFIG_FILENAME)));
			InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(DSA_ATTRIBUTE_CONFIG_FILENAME);
			byte[] bytes = IOUtils.toByteArray(inputStream);
			String configString = new String(bytes);
			// Read in configuration
			JSONObject config = new JSONObject(configString);
			return config;
		}
		
		
		/** 
		 * Read in XML file in Document doc according to the attribute config JSON object
		 * <p>Fail with an exception if the fields fail constraints</p>
		 * @param doc an XML document object containing the DSA XML file
		 * @param config a JSONObject containing the DSA ontology
		 * @return DSA metadata fields in a metadata JSONObject
		 */
		private JSONObject extractSearchFields(Document doc, JSONObject config) throws Exception {
			//TODO: if a configured field doesn't exist, should we fail?  For now, it might add a null?  Check this.
			JSONObject dsaMetadata = new JSONObject();
			
			if (config.has("attributes")) {
				JSONArray attributes = config.getJSONArray("attributes");

				int n = attributes.length();
				for (int i=0; i<n; i++) {
					JSONObject attribute = attributes.getJSONObject(i);
					
					// Get the compulsory fields from the attribute's config.  
					// If they don't exist, method will throw exception.
					String name = attribute.getString("name");

					String namespaceURI = attribute.getString("xmlNamespaceURI");
					String localName = attribute.getString("xmlLocalNodeName");
					String valueType = attribute.getString("valueType");
					
					// Read in attributes from the XML doc depending on valueType
					NodeList nodelist = doc.getElementsByTagNameNS(namespaceURI, localName);
					
					if (valueType.equals("array")) {
						// The attributes may be a list -- get all of them
						JSONArray val = new JSONArray();
						int m = nodelist.getLength();
						
						for (int j=0; j<m; j++) {
							Node node = nodelist.item(j);
							if (attribute.has("xmlLocalAttribute")) {
								String a = attribute.getString("xmlLocalAttribute");
								Element el = (Element)node;
								val.put(el.getAttribute(a));
							} else {
								val.put(node.getTextContent());
							}
						}
						dsaMetadata.put(name, val);
						
					} else { // valueType is a simple variable; make it a string
						// Assume that there is only one element; get the first one in the list
						Node node = nodelist.item(0);
						String val = new String();
						if (attribute.has("xmlLocalAttribute")) {
							String a = attribute.getString("xmlLocalAttribute");
							Element el = (Element)node;
							if(el == null) {
								LOGGER.error("extractSearchFields: el is null");
							} else {
								val = el.getAttribute(a);
							}	
						} else {
							val = node.getTextContent();
						}
						dsaMetadata.put(name, val);
						
					}				
				}
			}	
			
			// Return the extracted search fields
			return dsaMetadata;
		}
	/**
	 * Parse a string representation of an XML file into a Document object
	 * @param xml a string containing an XML document
	 * @return a Document object encapsulating the input string
	 * @throws Exception
	 */
	public static Document loadXMLFromString(String xml) throws Exception
	{
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    return builder.parse(is);
	}
	/**
	 * Converts xml document to string
	 * 
	 * @param doc
	 * @return a string representation of the input Document
	 * @throws TransformerException
	 */
	public static String convertXMLDoctoString(Document doc) throws TransformerException
	{
		DOMSource domSource = new DOMSource(doc);
	    StringWriter writer = new StringWriter();
	    StreamResult result = new StreamResult(writer);
	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.transform(domSource, result);
	    return writer.toString();
	}
	
	/**
	 * Converts a byte array to an XML Document
	 * 
	 * @param dsaBytes a byte array containing a DSA XML document
	 * @return a Document representation of the input byte array XML
	 * @throws TransformerException
	 */
	//convert byte array to xml document
	public static Document convertByteArraytoXMLDoc(byte[] dsaBytes) throws ParserConfigurationException, SAXException, IOException{
		InputStream dsa_xml_stream = new ByteArrayInputStream(dsaBytes);
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(dsa_xml_stream);
		
		return doc;
	}
	
	

		
	/**
     * Search the DSA Store based on a set of configured search fields from the DSA XML. 
     * <p>The search type will be dependent on the search field. The input is a string JSON representation of either DNF or CNF of search criteria, and a boolean value, TRUE for long results and FALSE for short results. The return will be an array of strings in JSON format containing the search fields associated with an individual DSA (when boolean is TRUE) or an array of dsa ids as strings (when boolean is FALSE).</p>
     * @param longResultFlag 
	 * 			if true, returns an array of metadata entries; if false, returns just an array of dsa_ids
	 * @param searchString
	 * 			correctly formatted JSON search string 
	 * @return DSA metadata of DSAs matching the search parameters.  If longResultFlag is true, returns all metadata for each entry; if it's false, returns just the DSA IDs.
	 * 
	 */
	@ApiOperation(notes = "Search the DSA Store based on a set of configured search fields from the DSA XML. The search type will be dependent on the search field. The input is a string JSON representation of either DNF or CNF of search criteria, and a boolean value, TRUE for long results and FALSE for short results. The return will be an array of strings in JSON format containing the search fields associated with an individual DSA (when boolean is TRUE) or an array of dsa ids as strings (when boolean is FALSE).", value = "search DSA")
	@ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.POST, value="/searchDSA/{longResultFlag}")
    public ResponseEntity<List<String>> searchDSA(@PathVariable boolean longResultFlag, @RequestBody @ApiParam(defaultValue="hello") String searchString ) {
		List<String> searchResult = new ArrayList<String>();
        HttpStatus httpstatus;
        ResponseEntity<List<String>> response;
        
        DSAQuery configuredQuery = ontology.configureQuery(searchString);
        
        if (configuredQuery.isValid()) {
	        try {
	        	searchResult = dsaStore.advancedSearchDSA(configuredQuery, longResultFlag);
	        	httpstatus = HttpStatus.OK;
	        } catch (Exception e) {
	        	// TODO: clean up the way errors are handled.
	        	searchResult = new ArrayList<String>();
	        	searchResult.add("Error running search");
	        	httpstatus = HttpStatus.NOT_FOUND;
	        	e.printStackTrace();
	        }
	        response = new ResponseEntity<List<String>>(searchResult, httpstatus);
        } else {
        	httpstatus = HttpStatus.BAD_REQUEST;
        	// TODO: clean up the way errors are handled.
        	List<String> errors = new ArrayList<String>();
        	errors.add(configuredQuery.getValidationErrorsAsString());
        	response = new ResponseEntity<List<String>>(errors, httpstatus);
        }    
	   return response;
    }
	
	
	/**
	 * Persist a DSA in the DSA Store.
	 * 
	 * @param multipartFile
	 * 			an XML file containing the DSA
	 * @return DSA ID of the created DSA
	 */
	@ApiOperation(notes = "Persist a DSA in the DSA Store. The input is an XML file containing the DSA; The API shall return a unique DSA identifier.", value = "create DSA")
	@ApiResponses(value = {
            //@ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.PUT, value="/createDSA")
	public ResponseEntity<String> createDSA(@RequestParam("DSAFile") MultipartFile multipartFile) { 	
		
        String resp;
        HttpStatus httpstatus;
        ResponseEntity<String> response;
        byte[] dsaXMLBytes = null;
        Boolean validXML = false;
        
        
        // Read in input file
        try {
        	String name = multipartFile.getOriginalFilename();

			LOGGER.info("!!!!!!!!!File name: "+name);
			dsaXMLBytes = multipartFile.getBytes();
        } catch (Exception e){
        	httpstatus = HttpStatus.BAD_REQUEST;
        	resp = "Error reading input file";
        }
        
        // Parse and validate XML in the input file
        
        if (dsaXMLBytes != null) {
			try {
				LOGGER.info("Reading XML file into DOM ");
				Document doc = convertByteArraytoXMLDoc(dsaXMLBytes);	
	            
				LOGGER.info("Validating XML document");
				validXML = validateXMLDoc(doc);
			} catch (Exception e) { //XML is invalid
				LOGGER.info("Exception during XML validation");
				e.printStackTrace();
				httpstatus = HttpStatus.BAD_REQUEST;
				resp = "XML file failed schema validation.";
			}
        } else {
        	httpstatus = HttpStatus.BAD_REQUEST;
        	resp = "Error reading input file";
        }
        
		// Extract metadata from XML document
    	DSAMetadata dsaMetadataObj = new DSAMetadata();
        if (validXML) {
        	try {	
				Document doc = convertByteArraytoXMLDoc(dsaXMLBytes);
		        LOGGER.info("Reading attribute config ");
				JSONObject config = readAttributeConfig();
				
				LOGGER.info("Extracting search fields ");
				JSONObject dsaMetadataJS = extractSearchFields(doc, config);
				LOGGER.info("!!!!! Extracted metadata: " + dsaMetadataJS.toString());

				LOGGER.debug("Building dsaMetadata object");
				dsaMetadataObj = ontology.configureMetadata(dsaMetadataJS);
				
        	} catch (Exception e) {
        		dsaMetadataObj.setValidity(new Boolean(false));
        		dsaMetadataObj.addValidationError("Exception occurred during XML validation");
        		e.printStackTrace();
            	LOGGER.info("Exception occurred during XML validation");
        		httpstatus = HttpStatus.BAD_REQUEST;
        		resp = "Errors extracting metadata from XML file " + e.getMessage();
        	}
        } else {	
          	dsaMetadataObj.setValidity(false);
          	dsaMetadataObj.addValidationError("XML file failed schema validation.");
        	LOGGER.info("Failed XML validation");
        	httpstatus = HttpStatus.BAD_REQUEST;
			resp = "XML file failed schema validation.";
			LOGGER.info("Validity is: " + dsaMetadataObj.isValid());
		}	
        
		//DSAMetadata and DSAXML objects store metadata and xml in two JSON documents in MongoDB
        if (dsaMetadataObj.isValid()) {	
        	try{
        		// TODO: add error checking in createDPO 
            	DSAXML dsaXMLObj = new DSAXML();
            	dsaXMLObj.setId(dsaMetadataObj.getId());
            	dsaXMLObj.setDsaXML(dsaXMLBytes);
            	
            	// TODO: separate metadata store and data store
        		httpstatus = HttpStatus.OK; 
        		resp = dsaStore.createDSA(dsaMetadataObj, dsaXMLObj);
        	}catch (Exception e){
        		LOGGER.info("Error inserting DSA into database");
        		httpstatus = HttpStatus.BAD_REQUEST;
        		resp = "Error inserting DSA into the database " + e.getMessage();			
        	}

        } else {
        	httpstatus = HttpStatus.BAD_REQUEST;
        	LOGGER.info("Metadata validation returned the following errors:\n" + dsaMetadataObj.getValidationErrors());	
        	resp = "Metadata validation returned the following errors:\n" + dsaMetadataObj.getValidationErrors();	
        }	
        
		LOGGER.info("Response string is " + resp);
		response = new ResponseEntity<String>(resp, httpstatus);
		return response;
    }
	
	/**
	 * Replace the content of an existing DSA.
	 * @param multipartFile
	 * 			an XML file containing the DSA to be updated
	 * @return DSA ID of the updated DSA
	 */
	@ApiOperation(notes = "Replace the content of an existing DSA. The input is an XML file containing the DSA; The API shall return the unique DSA identifier.", value = "update DSA")
	@ApiResponses(value = {
            //@ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.PUT, value="/updateDSA")
	ResponseEntity<String> updateDSA(@RequestParam("DSAFile") MultipartFile multipartFile) throws ParserConfigurationException, SAXException, IOException {		
		
		String resp;
        HttpStatus httpstatus;
        ResponseEntity<String> response;
        byte[] dsaXMLBytes = null;
        Boolean validXML = false;
        
        
        // Read in input file
        try {
        	String name = multipartFile.getOriginalFilename();

			LOGGER.info("!!!!!!!!!File name: "+name);
			dsaXMLBytes = multipartFile.getBytes();
        } catch (Exception e){
        	httpstatus = HttpStatus.BAD_REQUEST;
        	resp = "Error reading input file";
        }
        
        // Parse and validate XML in the input file
        
        if (dsaXMLBytes != null) {
			try {
				LOGGER.info("Reading XML file into DOM ");
				Document doc = convertByteArraytoXMLDoc(dsaXMLBytes);	
	            
				LOGGER.info("Validating XML document");
				validXML = validateXMLDoc(doc);
			} catch (Exception e) { //XML is invalid
				LOGGER.info("Exception during XML validation");
				e.printStackTrace();
				httpstatus = HttpStatus.BAD_REQUEST;
				resp = "XML file failed schema validation.";
			}
        } else {
        	httpstatus = HttpStatus.BAD_REQUEST;
        	resp = "Error reading input file";
        }
        
		// Extract metadata from XML document
    	DSAMetadata dsaMetadataObj = new DSAMetadata();
        if (validXML) {
        	try {	
				Document doc = convertByteArraytoXMLDoc(dsaXMLBytes);
		        LOGGER.info("Reading attribute config ");
				JSONObject config = readAttributeConfig();
				
				LOGGER.info("Extracting search fields ");
				JSONObject dsaMetadataJS = extractSearchFields(doc, config);
				LOGGER.info("!!!!! Extracted metadata: " + dsaMetadataJS.toString());

				LOGGER.debug("Building dsaMetadata object");
				dsaMetadataObj = ontology.configureMetadata(dsaMetadataJS);
				
        	} catch (Exception e) {
        		dsaMetadataObj.setValidity(new Boolean(false));
        		dsaMetadataObj.addValidationError("Exception occurred during XML validation");
        		e.printStackTrace();
            	LOGGER.info("Exception occurred during XML validation");
        		httpstatus = HttpStatus.BAD_REQUEST;
        		resp = "Errors extracting metadata from XML file: " + e.getMessage();
        	}
        } else {	
          	dsaMetadataObj.setValidity(false);
          	dsaMetadataObj.addValidationError("XML file failed schema validation.");
        	LOGGER.info("Failed XML validation");
        	httpstatus = HttpStatus.BAD_REQUEST;
			resp = "XML file failed schema validation.";
			LOGGER.info("Validity is: " + dsaMetadataObj.isValid());
		}	
        
        // Perform the update
        // DSAMetadata and DSAXML objects store metadata and xml in two JSON documents in MongoDB
        if (dsaMetadataObj.isValid()) {	
        	try{		
				String id = dsaMetadataObj.getId();
				LOGGER.info("Deleting the existing DSA with id " +id);

				if( dsaStore.readDSA(id) != null){
					dsaStore.deleteDSA(id);
					
					DSAXML dsaXMLObj = new DSAXML();
				    dsaXMLObj.setId(id);
				    dsaXMLObj.setDsaXML(dsaXMLBytes);
				    
				    httpstatus = HttpStatus.OK;
					resp = dsaStore.createDSA(dsaMetadataObj, dsaXMLObj);
				} else { // No such DSA exists in the store
					httpstatus = HttpStatus.NOT_FOUND;
					resp = "No DSA with this ID exists in the DSA Store";
				}			
				//TODO: differentiate between the different exceptions
        	} catch (Exception e) {
        		httpstatus = HttpStatus.BAD_REQUEST;
        		resp = "Error updating metadata in the repository: " + e.getMessage();
        	}
        } else {
        	httpstatus = HttpStatus.BAD_REQUEST;
        	LOGGER.info("Metadata validation returned the following errors:\n" + dsaMetadataObj.getValidationErrors());	
        	resp = "Metadata validation returned the following errors:\n" + dsaMetadataObj.getValidationErrors();	
        }		
        			
		response = new ResponseEntity<String>(resp, httpstatus);
		return response;
    }
	
	/**
	 * Retrieves the DSA identified by its id
	 * @param dsaId the unique DSA identifier
	 * @return The XML file identified by the dsa ID
	 */
	@ApiOperation(notes = "Retrieves the DSA identified by its id. The input is a string uniquely identifying the DSA document to be retrieved; The API shall return a file (in byte array format) containing the DSA document identified by its id in the DSA Store.", value = "read DSA")
	@ApiResponses(value = { 
            //@ApiResponse(code = 302, message = "Found"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.GET, value="/readDSA/{dsaId}")
	public ResponseEntity<byte[]> readDSA(@PathVariable String dsaId) {  
		ResponseEntity<byte[]> response;
		
		try {
			DSAXML dsaXMLObj = dsaStore.readDSA(dsaId);
			byte[] dsaXML = dsaXMLObj.getDsaXML();
			response = new ResponseEntity<byte[]>(dsaXML,
					HttpStatus.OK);
		} catch (Exception e) {
			response = new ResponseEntity<byte[]>(new byte[0],
					HttpStatus.NOT_FOUND);
		}
        return response;		                
    }
	

	/**
	 * Delete a DSA, by its identifie
	 * @param dsaId the unique DSA identifier
	 * @return nothing; just an HttpStatus
	 */
	@ApiOperation(notes = "Delete a DSA, by its identifier. The input is a string uniquely identifying the DSA document to be deleted; The return refers to the HTTP Message status.", value = "delete DSA")
	@ApiResponses(value = { 
            @ApiResponse(code = 204, message = "Content Not Found"),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 500, message = "Internal Server Error") 
            })
	@RequestMapping(method=RequestMethod.DELETE, value="/deleteDSA/{dsaId}")
	public ResponseEntity<String> deleteDSA(@PathVariable String dsaId) {
	    HttpStatus httpstatus;
	    String resp;
	    ResponseEntity<String> response;
	    try {
	    	dsaStore.deleteDSA(dsaId);
	    	LOGGER.info("delete the DSA " + dsaId);
	    	resp = "Delete successful";
	    	httpstatus = HttpStatus.OK;
	    } catch (Exception e) {
	    	resp = "Error deleting DSA. " + e.getMessage();
	    	httpstatus = HttpStatus.NOT_FOUND;
	    }
	    response = new ResponseEntity<String>(resp, httpstatus); 
		return response;
    }
	
/*	*//**
	 * stubSearchDSA
	 * @param longResultFlag
	 * @param searchString
	 * @return
	 *//*

	//@Value("${c3isp.metadata.collection}")
	//private String mycollection;
	
	@ApiOperation(notes = "Search the DSA Store based on a set of configured search fields.  Stub method that returns everything in the DB.", value = "Stub Search DSA")
	@RequestMapping(method=RequestMethod.POST, value="stubSearchDSA/{longResultFlag}")
    public ResponseEntity<List<String>> stubSearchDSA(@PathVariable boolean longResultFlag, @RequestBody @ApiParam(defaultValue="hello") String searchString ) {
		
		//TODO: figure out the autowired config
		LOGGER.info("!!!!!DEBUGGING ONTOLOGY");
		LOGGER.info("Ontology is: " + ontology.toString());
		
		
		ResponseEntity<List<String>> response;
		List<String> searchResult = new ArrayList<String>();
		try{
          searchResult = dsaStore.searchDSA(searchString, longResultFlag);
          response = new ResponseEntity<List<String>>(searchResult, HttpStatus.OK);
		}catch(Exception e){
			response = new ResponseEntity<List<String>>(searchResult, HttpStatus.NOT_FOUND);
		}
        //TODO
        return response;
    }*/
	
}
