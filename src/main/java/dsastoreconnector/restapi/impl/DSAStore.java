/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dsastoreconnector.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dsastoreconnector.restapi.types.DSAXML;
import dsastoreconnector.restapi.types.DSAMetadata;
import dsastoreconnector.restapi.types.DSAQuery;

// The two following imports are deemed necessary by Eclipse for some reason
import dsastoreconnector.restapi.impl.DSAXMLRepository;
import dsastoreconnector.restapi.impl.DSAMetadataRepository;

/**
 * Representation of the Data Sharing Agreement (DSA) Store
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */

@Service
public class DSAStore {

    @Autowired DSAXMLRepository dsaXMLRep;
    @Autowired DSAMetadataRepository dsaMetadataRep;
    
    private final static Logger LOGGER = LoggerFactory.getLogger(DSAStore.class);
	
    // TODO: Ensure atomicity of delete/create/update operations -- what if delete metadata succeeds but delete xml fails?
    
	/**
	 * Creates a DSA entry in the repository
	 * @param dsaMetadataObj a <code>DSAMetadata</code> object containing the DSA Metadata
	 * @param dsaXMLObj a byte array containing the DSA XML file
	 * @return dsaId the unique ID for this DSA 
	 * @throws Exception
	 */
	public String createDSA(DSAMetadata dsaMetadataObj, DSAXML dsaXMLObj) throws Exception {		   
		   DSAMetadata dM = null;
		   DSAXML dX = null;
		   
		   String dsaId = dsaXMLObj.getId();
		
		   // Insert metadata  
		   try {
			   dM = dsaMetadataRep.insert(dsaMetadataObj);
			} catch (Exception e) {
				e.printStackTrace();
				Exception ne = new Exception("Error inserting DSA metadata into the repository" + e.getMessage());
				throw ne;
			}		
		   // Insert XML 
		   try {
			   dX = dsaXMLRep.insert(dsaXMLObj);
			} catch (Exception e) {
				e.printStackTrace();
				Exception ne = new Exception("Error inserting DSA XML into the repository" + e.getMessage());
				throw ne;
			}
		   return dsaId;
	}
	
	/**
	 * Retrieves the Data Sharing Agreement by its identifier
	 * @param dsaId the unique ID of the DSA to be retrieved
	 * @return a <code>DSAXML</code> object encapsulating the DSA XML
	 * @throws Exception
	 */
	public DSAXML readDSA(String id) throws Exception {
		   DSAXML dsaXMLObj = null;
		   try{
			   dsaXMLObj = dsaXMLRep.findById(id);
		   } catch (Exception e) {
			   e.printStackTrace();
			   Exception ne = new Exception("Error retrieving DSA XML " + e.getMessage());
			   throw ne;
		   }		   	   
		   return dsaXMLObj;
	}
	
	/**
	 * Deletes a DSA from the repository by its identifier.
	 * 
	 * @param dsaId the identifier of the DSA to be deleted 
	 * @throws Exception
	 */
	public void deleteDSA(String id) throws Exception {
		try {
			DSAMetadata dsaMetadataObj = dsaMetadataRep.findById(id);
			if (null == dsaMetadataObj) {
				Exception ne = new Exception("No metadata found with id " + id);
				throw ne;
			} else {
				dsaMetadataRep.delete(dsaMetadataObj);
			}	
		} catch (Exception e) {
			e.printStackTrace();
			Exception ne = new Exception("Error deleting DSA metadata: " + e.getMessage());
			throw ne;
		}	
		try {
			DSAXML dsaXMLObj = dsaXMLRep.findById(id);
			if (null == dsaXMLRep) {
				Exception ne = new Exception("No xml representation found with id " + id);
				throw ne;
			} else {	
				dsaXMLRep.delete(dsaXMLObj);
			} 	
		} catch (Exception e) {
			e.printStackTrace();
			Exception ne = new Exception("Error deleting XML from repository: " + e.getMessage());
			throw ne;
		}	
	}
	
	
	/**
	 * Stub search on the DSA Metadata repository, for debugging only
	 * <p>Returns all entries in the database</p> 
	 * 
	 * @param metadataquery
	 * @param longResultFlag
	 * @return
	 */
	
	public List<String> searchDSA(String searchString, boolean longResultFlag){
		List<String> searchResult = new ArrayList<String>();
		if(longResultFlag == false){
			List<DSAXML> dsaXMLList = dsaXMLRep.findAll();
			for(int i = 0; i < dsaXMLList.size(); i++){
				if(dsaXMLList.get(i).getDsaXML()!= null){
					String id = dsaXMLList.get(i).getId();
					searchResult.add(id);	
				}				
			}		
		}else{
			List<DSAMetadata> dsaMetadataList = dsaMetadataRep.findAll();
			for(int i = 0; i < dsaMetadataList.size(); i++){
				if(dsaMetadataList.get(i).getDsaMetadataJS() != null){
					String dsaMetadata = dsaMetadataList.get(i).getDsaMetadataJS().toString();
					searchResult.add(dsaMetadata);	
				}
			}
		}
		return searchResult;
	}
	
	
	/**
	 * Implements search on the DSA Metadata repository 
	 * 
	 * @param metadataquery a <code>DSAQuery</code> object encapsulating the parsed JSON input query
	 * @param longResultFlag TRUE for long results (full metadata entries); FALSE for short results (DSA ids only)
	 * @return a <code>List</code> of search results
	 */ 
	public List<String> advancedSearchDSA(DSAQuery metadataquery, boolean longResultFlag) {		
		// Read query
		
		// Match query against config file -- is it the right format?
		// JSONObject metadataquery, JSONObject config 
		// TODO: validateQuery(metadataquery, config) 
		
		
		List<String> searchResult = new ArrayList<String>();
		List<DSAMetadata> dsaMetadataList = dsaMetadataRep.runQuery(metadataquery);

		if(longResultFlag == false){
			for(int i = 0; i < dsaMetadataList.size(); i++){
				if(dsaMetadataList.get(i).getDsaMetadataJS() != null){
					String id = dsaMetadataList.get(i).getId();
					searchResult.add(id);	
				}				
			}		
		}else{
			for(int i = 0; i < dsaMetadataList.size(); i++){
				if(dsaMetadataList.get(i).getDsaMetadataJS() != null){
					String dsaMetadata = dsaMetadataList.get(i).getDsaMetadataJS().toString();
					searchResult.add(dsaMetadata);	
				}
			}
		}
		return searchResult;
	}
	
	
}
