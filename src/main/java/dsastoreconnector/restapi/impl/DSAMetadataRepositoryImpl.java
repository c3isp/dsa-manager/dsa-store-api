/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dsastoreconnector.restapi.impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

//import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoTemplate;

import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

//import static org.springframework.data.mongodb.core.query.Criteria.where;
//import static org.springframework.data.mongodb.core.query.Update.update;
//import static org.springframework.data.mongodb.core.query.Query.query;

import dsastoreconnector.restapi.types.DSAMetadata;
import dsastoreconnector.restapi.types.DSAQuery;
import dsastoreconnector.restapi.impl.DSAMetadataRepositoryCustom;
/**
 * Implements the DSA Metadata Repository interface in a MongoDB repository
 * <p>In addition to the basic Repository methods, adds custom functionality for handling DSA queries
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */

//@RepositoryRestResource(collectionResourceRel = "dsastore", path = "dsastore")
@RepositoryRestResource
public class DSAMetadataRepositoryImpl implements DSAMetadataRepositoryCustom {
	
	@Autowired
	MongoTemplate mongoTemplate;
	
	private final static Logger LOGGER = LoggerFactory.getLogger(DSAMetadataRepository.class);
		
	
	/** parseQuery
	 * 
	 * Translates a CNF/DNF JSON query into a MongoDB query object
	 * <p>Assumes that the JSON query has been validated, and contains the following fields:
	 * Top level: "combining_rule" and "criteria"
	 * For each criteria: "attribute", "operator", "value" and "valueType"
	 * ("valueType" is added based on the metadata/query ontology file) </p>
	 * @param mquery	a <code>DSAQuery</code> object containing the DSA query
	 * @return a mongodb Query object containing the translated query 
	 */
	private Query parseQuery(DSAQuery mquery) {
		Query query = new Query();
		
		String METADATA_PREFIX = "dsaMetadataJS.map"; // the prefix needed to access the JSON structure in the database
		
		LOGGER.info("The JSON query is: " + mquery.toString());
		
		// translate the criteria in the JSON file to MongoDB query criteria
		JSONArray mcriteria = mquery.getCriteria();
		
		// Get the combining rule
		String combining_rule = mquery.getCombiningRule();
		
		// Parse out individual criteria
		List<Criteria> criteriaList = new ArrayList<Criteria>();

		for (int i=0; i<mcriteria.length(); i++) {
			JSONObject mc = mcriteria.getJSONObject(i);
			String attribute = mc.getString("attribute");
			String operator = mc.getString("operator");
			String value = mc.getString("value");
			String valueType = mc.getString("valueType");
			
			String ARRAY_SUFFIX ="";
			if (valueType.equals("array")) {
				ARRAY_SUFFIX = ".myArrayList";
			}
			
			if (operator.equals("eq") || operator.equals("in")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).is(value));
			} else if (operator.equals("ne") || operator.equals("nin")) {
				criteriaList.add(Criteria.where(METADATA_PREFIX+"."+attribute+ARRAY_SUFFIX).ne(value));
			}
		} //TODO Add operators and type support for dates, etc.
		
		// Combine the criteria with the combining rules
		Criteria queryCriteria = new Criteria();
		if (combining_rule.equals("and")) {
			queryCriteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		} else if (combining_rule.equals("or")) {
			queryCriteria = new Criteria().orOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		} else { //default is "and"
			queryCriteria = new Criteria().andOperator(criteriaList.toArray(new Criteria[criteriaList.size()]));
		}
	
		// Construct the query
		if (criteriaList.size() > 0) {
			query.addCriteria(queryCriteria);
		} // else, with no criteria, it's a blank query that returns everything
		
		return query;
	}

	/** 
	 * Runs a CNF/DNF query on the DSA metadata repository
	 * 
	 * @param metadataquery a DSAQuery object containing the DSA metadata query
	 * @return a List of DSAMetadata objects matching the query
	 */
	public List<DSAMetadata> runQuery(DSAQuery metadataquery)	{
			//DSASearchableMetadata m = new DSASearchableMetadata();
			List<DSAMetadata> list;
			//list.add(m);
			Query query = parseQuery(metadataquery);
			
			LOGGER.info("Parsed query is " + query.toString());
			list = mongoTemplate.find(query, DSAMetadata.class);
			
			return list;
	}
}
