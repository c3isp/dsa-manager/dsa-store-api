/*******************************************************************************
 * Copyright (c) 2019 University of Kent
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *******************************************************************************/
package dsastoreconnector.restapi.types;

import java.util.LinkedList;

import org.json.JSONObject;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.*;
import org.springframework.beans.factory.annotation.*;

/**
 * Represents the DSA Metadata object
 * @author Joanna Ziembicka, Wenjun Fan
 * @author University of Kent
 *
 */

@Document(collection = "dsaCollection")
//@Document(collection="${c3isp.metadata.collection}") // TODO: figure out how to configure collection name from config file
public class DSAMetadata {     		
	

	
	@Id private String internalId;
	private String id;
	private JSONObject dsaMetadataJS;
	private boolean isValid;
	private LinkedList<String> validationErrors;
	
	/**
	 * Constructor
	 * <p>Creates an empty DSAMetadata object, which is valid by default</p>
	 */
	public DSAMetadata() {
		isValid = true;
		validationErrors = new LinkedList<String>();
	}
	
	/**
	 * Reports whether this metadata is valid with respect to its ontology
	 * @return <code>true</code> if this set of metadata is valid, <code>false</code> otherwise
	 */
	public boolean isValid() {
		return isValid;
	}
	
	/**
	 * Sets the validity status of this set of metadata
	 * @param validity <code>true</code> if valid, <code>false</code> if not
	 */
	public void setValidity(boolean validity) {
		isValid = validity;
	}
	
	/** 
	 * Gets all the errors reported during validation of this set of metadata
	 * @return a <code>LinkedList</code> of <code>Strings</code> containing all the errors reported during validation of this set of metadata
	 */
	public LinkedList<String> getValidationErrors() {
		return validationErrors;
	}
	
	/** 
	 * Gets all the errors reported during validation of this set of metadata
	 * @return a <code>String</code> containing all the errors reported during validation of this set of metadata
	 */
	public String getValidationErrorsAsString() {
		String errors = "";
		for (int i=0; i<validationErrors.size(); i++) {
			errors = errors + validationErrors.get(i) + "\n";
		}
		return errors;
	}
	
	/** Adds a validation error
	 * @param error a <code>String</code> containing an error reported during validation of this set of metadata
	 */
	public void addValidationError(String error) {
		validationErrors.add(error);
	}
	
	/**
	 * Gets the unique identifier associated with this set of metadata
	 * @return a unique id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Sets the unique identifier associated with this set of metadata
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
		this.internalId = id + ".metadata";
	}

	/**
	 * Retrieves the <code>JSONObject</code> representation of this set of metadata
	 * @return the <code>JSONObject</code> representation of this set of metadata
	 */
	public JSONObject getDsaMetadataJS() {
		return dsaMetadataJS;
	}
	
	/**
	 * Sets the internal <code>JSONObject</code> representation of this set of metadata
	 * @param dsaMetadataJS the <code>JSONObject</code> representation of this set of metadata
	 */
	public void setDsaMetadataJS(JSONObject dsaMetadataJS) {
		this.dsaMetadataJS = dsaMetadataJS;
	}

}
